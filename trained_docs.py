from io import open
import json

from snips_nlu import SnipsNLUEngine, load_resources
from snips_nlu.default_configs import CONFIG_EN

load_resources(u"en")

# engine = SnipsNLUEngine(config = CONFIG_EN)
# with open("dataset.json") as f:
#     dataset = json.load(f)
engine = SnipsNLUEngine.from_path("trained_docs")
# engine.fit(dataset)

values = engine.parse(u"Please give me some lights in the entrance !")
print(json.dumps(values, indent=2))

# snips-nlu train dataset.json trained_docs


"""
You can persist the engine with the following API:

engine.persist("path/to/directory")

And load it:

loaded_engine = SnipsNLUEngine.from_path("path/to/directory")

loaded_engine.parse(u"Turn lights on in the bathroom please")

Alternatively, you can persist/load the engine as a bytearray:

engine_bytes = engine.to_byte_array()
loaded_engine = SnipsNLUEngine.from_byte_array(engine_bytes)

"""

